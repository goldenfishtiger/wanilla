FROM fedora:latest

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG http_proxy
ARG https_proxy

# https://medium.com/nttlabs/ubuntu-21-10-and-fedora-35-do-not-work-on-docker-20-10-9-1cd439d9921
#ARG c3wahost=https://github.com/AkihiroSuda/clone3-workaround/releases/download/v1.0.0/
#ADD ${c3wahost}/clone3-workaround.x86_64 /clone3-workaround
#RUN chmod 755 /clone3-workaround
#SHELL ["/clone3-workaround", "/bin/bash", "-c"]

RUN set -x \
	&& dnf install -y \
		util-linux \
		procps-ng \
		iputils \
		net-tools \
		diffutils \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

RUN set -x \
	&& useradd user
ARG TARGET=wanilla
COPY ${TARGET} /home/user/${TARGET}
RUN set -x \
	&& /usr/bin/chown -R user:user /home/user/${TARGET}
USER user
WORKDIR /home/user
USER root

CMD ["bash", "-c", "wanilla/test.sh && sleep 9999"]

